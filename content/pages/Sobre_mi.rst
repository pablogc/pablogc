========
Sobre mi
========

+---------------------------------------+
| Datos Personales                      |
+=======================================+
| Pablo Manuel García Corzo             |
+---------------------------------------+
| 27/08/1983                            | 
+---------------------------------------+
| pablo.garcia.corzo@gmail.com          |
+---------------------------------------+
| 616 26 77 36                          |
+---------------------------------------+


Presentación y aptitudes
========================

Físico de formación, comencé mi carrera ligado al Grupo de Física Nuclear de la UCM donde comencé a interesarme por la parte más técnica del trabajo ligada a la programación (**python**, **optimización evolutiva**, **C/C++**, **fortran**, **LaTeX**) y administración de sistemas **Linux** (**clusters de alto rendimiento**, **GPU/CUDA**, **paralelización**). De mi afición por el mundo Linux surgió también un interés por todo lo relacionado con el copyleft y de ahí mi participación en diversos proyectos y actividades de asociaciones de estudiantes (*Grupo de Software Libre*, *editorial Alqua*, *Jornadas SAGE/Python*, *Semana de la Ciencia*).

En 2012 salgo del mundo académico y comienzo a trabajar para BlueTC como consultor externo en Ericsson R&D. Allí empiezo a desarrollar mi actividad principalmente como desarrollador en Python de sistemas de automatización de instalaciones y pruebas para integración contínua. Allí pude colaborar estrechamente con el equipo de integración donde adquirí amplia experiencia en herramientas de automatización (**Jenkins**, **puppet**) y control de versiones (**git**, **Gerrit**). Además de los conocimientos técnicos, valoro mucho la experencia de haber trabajado con metodología **Agile** y lo que de ello aprendí a la hora de coordinar equipos de trabajo.

En Diciembre de 2014 dejo de trabajar con Ericsson como cliente para formar parte del área de Soluciones de BlueTC. Mi primer proyecto en este área consistiría en el montaje de una testbed para realizar demostraciones de un IDS para soluciones IMS (**Clearwater**) en la nube (**Openstack**). Para este proyecto aprendí a usar **vagrant** como herramienta para tratar la infraestructura como código, diversas herramientas de gestión de configuraciones (**chef**, **ansible**) y a manejar redes virtuales (**libvirt**, **Openvswitch**, **vtap**) en profundidad. Esa demostración la presenté en Marzo de 2015 en el *Mobile World Congress* en Barcelona.

En la actualidad, coordino un equipo de tres personas en un proyecto de monitorización (**Nagios**) de entornos de cientos de servidores (**Linux** y **Solaris**) críticos para el cliente donde la automatización precisa (mediante recetas en **ansible**) y los bancos de pruebas (**vagrant**, **KVM**) son piezas fundamentales.


Experiencia Profesional
=======================

Solutions Architect @ BlueTC
----------------------------

*BlueTC (Diciembre 2014 - Actualidad)*

En este puesto me he especializado en infraestructura como servicio (IAAS), virtualización (KVM, libvirt, vagrant) y cloud computing (openstack). 
He trabajado en la adaptación de soluciones de seguridad IMS (Clearwater) a entornos virtualizados. 
He desplegado y configurado entornos de monitorización con Nagios sobre grandes plataformas de producción con cientos de servidores.

Configuration Manager @ Ericsson R&D
------------------------------------

*BlueTC (Octubre 2012 - Diciembre 2014)*

Como consultor externo trabajé en el desarrollo en Python de software autónomo para la instalación y configuración de CUDB y componentes OMP y CBA (Python). 
He participado en el diseño, implementación y productificación de una herramienta para la generación de plantillas de configuración para los componentes de CUDB (Python, Jinja, ).
He diseñado la estrategia de integración contínua de GAIS y CUTe Builder (git/gerrit+Jenkins) y he participado en la implementación de Code Review con Gerrit para el desarrollo de CUDB.


Investigador @ GFN-UCM
----------------------

*Grupo de Física Nuclear, Universidad Complutense de Madrid (2007 - Octubre 2012)*

He compaginado labores de administrador de sistemas con investigación en Física Nuclear enfocada a la imágen médica. 

Como **administrador de sistemas**, he montado, configurado y administrado diversas estaciones de trabajo GNU-Linux (CentOS, Fedora, Debian, RedHat), servidores web y de correo, clusters de alto rendimiento (SUN Grid Engine)ç, soluciones de almacenamiento RAID y servidores de cálculo basados en GPU. 

Como **investigador**, he trabajado con grandes códigos Monte Carlo de interacción Radiación-Materia (DPM, Penelope), reconstrucción de imagen con métodos iterativos (OSEM3D) y diversos métodos de optimización (especialmente algoritmos evolutivos).


Otra experiencia y proyectos personales
---------------------------------------

Editorial Alqua
_______________

Entre 2007 y 2012 me he involucrado en el proyecto Alqua, una editorial pionera en la publicación de contenidos docentes bajo licencias tipo *Creative Commons*.

En este proyecto he escrito, coescrito y publicado cuatro libros de texto de física y matemáticas (ver publicaciones).  


Desarrollador de aplicaciones Android
_____________________________________

He desarrollado algunos juegos para android usando las librerías pygame4android.

Perfil de desarrollador: https://play.google.com/store/apps/developer?id=OzrocDroid


Programación de juegos en Python
________________________________

He participado en varias ocasiones en el concurso de programación de juegos con python en una semana Pyweek: https://pyweek.org/u/ozroc/

 * *Edición Agosto 2010*: un pequeño juego de billar basado en las librerías de pygame. \emph{Hadron Collider award}
 * *Edición Mayo 2012*: un pequeño juego con interfaz de usuario basada en seguimiento de color con webcam. \emph{Arts \& Crafts award}

Educación
=========

Graduado en Física por la Universidad Complutense de Madrid
-----------------------------------------------------------

Mi Trabajo de Fin de Grado consistió en la implementación en Python de un sistema de seguimento de objetos con webcam basado en color para laboratorios de mecánica y física general.

Cursos y workshops
__________________

II Jornada de SAGE/Python
.........................

*Universidad de La Rioja, 2011*

Workshop sobre SAGE/Python para educación. Ponente.

https://www.unirioja.es/dptos/dmc/sage2011/

I Jornada de SAGE/Python
........................

*Universidad Complutense de Madrid, 2010*

Workshop sobre SAGE/Python para educación. Organizador y ponente. 

http://jacobi.fis.ucm.es/david/jornada-sage-python/

II Workshop en Aplicaciones de Nuevas Arquitecturas de Consumo y Altas Prestaciones
...................................................................................

*Universidad Rey Juan Carlos, 2009*

Applicaciones de alto rendimiento sobre **GPU-CUDA**.  


Publicaciones
-------------

Off-Line PET Study of Proton Beam Activated Metals
__________________________________________________

*International workshop on bio-medical applications of micro-PET, (Septiembre 2010)* 
Autores: Pablo Manuel García Corzo, Jacobo Cal-Gonzalez, Esteban Picado, Samuel España, Esther Vicente, Elena Herranz, Luis Fraile, Jose Udias, Juan Jose Vaquero, A. Muñoz

Iterative reconstruction of whole accelerator phase spaces for Intraoperative Radiation Therapy (IORT) from measured dose data
______________________________________________________________________________________________________________________________

*IEEE Nuclear Science Symposium Conference Record, (2011)*
Autores: Elena Herranz, Joaquin Lopez Herraiz, Jacobo Cal-Gonzalez, Pablo Manuel García Corzo, Pedro Guerra, Jose Udias 


A general framework to study positron range distributions
_________________________________________________________

*EEE Nuclear Science Symposium and Medical Imaging Conference (NSS/MIC) (2011)* 
Autores: Jacobo Cal-Gonzalez, Joaquin Lopez Herraiz, Samuel España, Pablo Manuel García Corzo, Jose Udias

Rendimiento de códigos de interés en Bioingeniería paralelizados para su ejecución multi-ordenador y multi-núcleo
_________________________________________________________________________________________________________________

*Congreso Anual de la Sociedad Española de Ingeniería Biomédica (2010)*

Autores: Joaquin Lopez Herraiz, Pablo Manuel García Corzo, Jacobo Cal-Gonzalez, Elena Herranz, Pedro Guerra, Jose Udias

Mecánica Lagrangiana
____________________

*Editorial Alqua (2009)*

Autores: Álvaro Hacar, Fabio Revuelta Peña, Israel Saeta Pérez, Pablo Manuel García Corzo, Enrique Barber

`<http://alqua.tiddlyspace.com/#[[Mec%C3%A1nica lagrangiana]]>`_

Cálculo Multivariable
_____________________

*Editorial Alqua (2007)* 

Autores: Joaquín Retamosa Granado, Pablo Manuel García Corzo

`<http://alqua.tiddlyspace.com/#[[C%C3%A1lculo multivariable]]>`_


Proyecto Physthones
___________________

*Editorial Alqua (2012)* ISBN:9788469432464

Autores: Pablo Manuel García Corzo

`<http://alqua.tiddlyspace.com/#[[Proyecto Physthones]]>`_


Mecánica y Ondas II
___________________

*Editorial Alqua (2011)* ISBN:9788469432457

Autores: Pablo Manuel García Corzo

`<http://alqua.tiddlyspace.com/#[[Mec%C3%A1nica%20y%20Ondas II]]>`_

