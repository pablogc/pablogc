#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Pablo Manuel Garc\xeda'
SITENAME = u'PabloMGC'
SITEURL = ''

THEME = "theme"

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'es'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('BlueTC', 'http://www.bluetc.es'),
         )

# Social widget
SOCIAL = (('LinkedIn', 'https://www.linkedin.com/in/pablomgc'),
          ('Academia.edu', 'http://independent.academia.edu/PabloMGarc%C3%ADaCorzo'),
          ('Google App Store', 'https://play.google.com/store/apps/developer?id=OzrocDroid'),
          )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
